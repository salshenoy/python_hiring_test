"""Main script for generating output.csv."""
import pandas as pd

'''
 Class stat_calculator: Contains dataframes which have the complete dataset and members with filters applied.
 The class contains member functions to read input data, calculate the different stats as per the formulaes below,
 (obtained from Wiki: https://en.wikipedia.org/wiki/Baseball_statistics#Commonly_used_statistics). 
 
 Calculation of Stats:
    AVG = (H / AB)
    OBP = (H + BB + HBP) / (AB + BB + HBP + SF)
    SLG = (TB / AB)
    OPS = OBP + SLG
    
 The results calcualated are then written to the output file (output.csv) stored in ./data/processed.
'''
class stat_calculator(object):

    '''
        Constructor
    '''
    def __init__(self):
        self.pitcher_data = pd.DataFrame([])
        self.analysis = pd.DataFrame(columns = ['SubjectId', 'Stat', 'Split', 'Subject', 'Value'])
        self.data_path = './data/raw/pitchdata.csv'
        self.output_path = './data/processed/output.csv'
        self.combinations_text = './data/reference/combinations.txt'

        self.rhp = pd.DataFrame([])
        self.lhp = pd.DataFrame([])
        self.rhh = pd.DataFrame([])
        self.lhh = pd.DataFrame([])

    '''
        Function to read in the dataset, we then apply filter to create data sets for right and left Pitcher and
        Hitter Side.
    '''
    def read_csv(self):
        self.pitcher_data = pd.read_csv(self.data_path)
        self.pitcher_data = self.pitcher_data.drop(['GameId', 'PrimaryEvent'], axis=1)
        self.rhp = self.pitcher_data[self.pitcher_data['PitcherSide'] == 'R']
        self.lhp = self.pitcher_data[self.pitcher_data['PitcherSide'] == 'L']
        self.rhh = self.pitcher_data[self.pitcher_data['HitterSide'] == 'R']
        self.lhh = self.pitcher_data[self.pitcher_data['HitterSide'] == 'L']

    '''
        Read the combinations text file to calculate stats for different groups mentioned.
    '''
    def read_combinations(self):
        stats_to_measure = open(self.combinations_text, 'r')
        combinations = stats_to_measure.readlines()
        for i in range(1, len(combinations)):
            combination = combinations[i]
            combination = combination.split(',')
            self.calculate_stats(combination[0], combination[1], combination[2].strip())

    '''
        Create the dataframe with all stats calculated
    '''
    def report(self, dataframe, subject, stat, split):
        dataframe = dataframe.reset_index()
        dataframe['SubjectId'] = dataframe[subject]
        dataframe['Stat'] = stat
        dataframe['Split'] = split
        dataframe['Subject'] = subject
        dataframe = dataframe.sort_values(by = ['SubjectId', 'Stat', 'Split', 'Subject'])
        self.analysis = self.analysis.append(dataframe)

    '''
        Depending on the subject, select the dataframe to process right / left,  hitter / pitcher
    '''
    def select_dataframe(self, split):
        dataframe_to_use = None
        if split == 'vs RHP':
            dataframe_to_use = self.rhp
        elif split == 'vs RHH':
            dataframe_to_use = self.rhh
        elif split == 'vs LHP':
            dataframe_to_use = self.lhp
        elif split == 'vs LHH':
            dataframe_to_use = self.lhh

        return dataframe_to_use

    '''
        Calculate the AVG
    '''
    def calculate_batting_average(self, dataframe, subject):
        dataframe = dataframe.groupby(subject).agg({'H': 'sum', 'PA': 'sum', 'AB': 'sum'})
        result = dataframe[dataframe['PA'] >= 25]
        result['Value'] = result['H'] / result['AB']
        result['Value'] = result['Value'].map('{:,.3g}'.format)
        return result

    '''
        Calculate OBP
    '''
    def calculate_obp(self, dataframe, subject):
        dataframe = dataframe.groupby(subject).agg(
            {'H':'sum', 'BB':'sum', 'HBP':'sum', 'AB':'sum', 'BB':'sum', 'SF':'sum', 'PA': 'sum'})
        result = dataframe[dataframe['PA'] >= 25]
        result['num'] = result['H'] + result['BB'] + result['HBP']
        result['den'] = result['AB'] + result['BB'] + result['HBP'] + result['SF']
        result['Value'] = result['num'] / result['den']
        result['Value'] = result['Value'].map('{:,.3g}'.format)
        return result

    '''
        Calculate SLG
    '''
    def calculate_slg(self, dataframe, subject):
        dataframe = dataframe.groupby(subject).agg({'TB': 'sum', 'PA': 'sum', 'AB': 'sum'})
        result = dataframe[dataframe['PA'] >= 25]
        result['Value'] = result['TB'] / result['AB']
        result['Value'] = result['Value'].map('{:,.3g}'.format)
        return result

    '''
        Calculate OPS
    '''
    def calculate_ops(self, dataframe, subject):
        dataframe = dataframe.groupby(subject).agg(
            {'H': 'sum', 'BB': 'sum', 'HBP': 'sum', 'AB': 'sum', 'BB': 'sum', 'SF': 'sum', 'PA': 'sum','TB': 'sum'})
        result = dataframe[dataframe['PA'] >= 25]
        result['num'] = result['H'] + result['BB'] + result['HBP']
        result['den'] = result['AB'] + result['BB'] + result['HBP'] + result['SF']
        result['obp'] = result['num'] / result['den']
        result['slg'] = result['TB'] / result['AB']
        result['Value'] = result['obp'].add(result['slg'])
        result['Value'] = result['Value'].map('{:,.3g}'.format)
        return result


    '''
        Function which selects which dataframe to use and which stats needs to be calculated.
    '''
    def calculate_stats(self, stat, subject, split):
        dataframe_to_use = self.select_dataframe(split)
        if dataframe_to_use is None:
            print ('Please provide the correct split parameter')
            return
        if stat == 'AVG':
            result = self.calculate_batting_average(dataframe_to_use, subject)
            self.report(result, subject, stat, split)
        elif stat == 'OBP':
            result = self.calculate_obp(dataframe_to_use, subject)
            self.report(result, subject, stat, split)
        elif stat == 'SLG':
            result = self.calculate_slg(dataframe_to_use, subject)
            self.report(result, subject, stat, split)
        elif stat == 'OPS':
            result = self.calculate_ops(dataframe_to_use, subject)
            self.report(result, subject, stat, split)
        else:
            print ('Stat entered is not valid')

    '''
        Write the final dataframe to the csv file
    '''
    def write_to_file(self):
        self.analysis = self.analysis.sort_values(by = ['SubjectId', 'Stat', 'Split', 'Subject'])
        self.analysis.to_csv(self.output_path, columns=['SubjectId', 'Stat', 'Split', 'Subject', 'Value'],
                             index = False, mode='w', encoding='utf-8')


def main():
    pitcher_stats = stat_calculator()
    pitcher_stats.read_csv()
    pitcher_stats.read_combinations()
    pitcher_stats.write_to_file()


if __name__ == '__main__':
    main()
